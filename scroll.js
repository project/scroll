
Drupal.behaviors.director = function(context) {
  var interval;
  $("body").mousemove(function(ev) {
    var x = ev.pageX;
    var y = ev.pageY;
    $("#coordinates").html("Column:"+x+" - Row:"+y);
  });
  $("div.scrollpage").click(function() {
    $("div.scrollpage").css('top', '0px');
    interval = setInterval(scroll,0);
  });
  function scroll() {
    $("div.scrollpage").animate({top:-1000000},1000000);  
  }
}
