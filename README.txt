$Id

NOTE: Make sure that your browser window is maximized,
and that when you take the upper left column and row readings
that you are paged all the way up.
-- Setup
Click the Scroll > Setup link.
Move the cursor arrow to the upper left corner of the bounding box,
and make a note of the Column and Row values.
Enter those values in the Upper left column and Upper left row
fields of the form.
Enter the file path (url) to the tutorial you wish to scroll.
Use sites/default/files/sample.html as a sample.
Click the Save button.
-- Run
The Run screen will appear.
Place the cursor anywhere in the bounding box, and click the mouse.
The selected tutorial will scroll.
-- Make a video
Download and install AviScreen Classic version 1.3 (it\'s free) from
http://www.bobyte.com/AviScreen/index.asp.
Fire up AviScreen and set the capture area to 480x360,
turn Show the cursor off, and Follow the cursor mode on.
Click Scroll > Run to reset the tutorial.
Move the AviScreen window off to the right side of the bounding box.
Make sure that AviScreen is the active window
Position the cursor in the center of the bounding box.
The Column and Row values will match the calculated values.
Hit Ctrl-Alt-S to start the AviScreen video recorder.
You may have to hit it more than once.
When the AviScreen wheel starts turning (lower left icons),
wait for three seconds, then click the mouse (without moving it).
When the tutorial has scrolled off the screen, hit Ctrl-Alt-F.
You may have to hit it more than once.
The video will start playing in your media player.
Go to AviScreen > Folder > Browse folder.
Click Compatibilty Files to make the captured video visible.
Rename it to something like My_Whatever_Tutorial.
Upload it to YouTube for exposure that will make you famous.
Enjoy !!!
